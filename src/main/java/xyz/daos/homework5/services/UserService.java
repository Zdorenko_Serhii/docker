package xyz.daos.homework5.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import xyz.daos.homework5.entity.User;
import xyz.daos.homework5.repository.UserRepository;

import java.util.List;

@Service
public class UserService {
    private UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public Page<User> getAllUsersPageable(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public List<User> getAllUsers() {
        return (List<User>) repository.findAll();
    }

    public User getUserById(int id) {
        return repository.getUserById(id);
    }

    public void saveOrUpdate(User user) {
        repository.save(user);
    }

    public void delete(int id) {
        repository.deleteById(id);
    }
}
