package xyz.daos.homework5.endpoint;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component("customHealthCheck")
public class HealthCheck implements HealthIndicator {
    @Value("${spring.application.name}")
    String name;

    @Override
    public Health health() {
        return Health.up().withDetail("applicationName", name).build();
    }
}