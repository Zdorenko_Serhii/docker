package xyz.daos.homework5.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xyz.daos.homework5.entity.User;
import xyz.daos.homework5.services.UserService;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    private UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping(params = {"page", "size"})
    public Page<User> getAllUsersPaginated(@RequestParam("page") int page,
                                           @RequestParam("size") int size) {
        return service.getAllUsersPageable(PageRequest.of(page, size, Sort.by("firstName")));
    }

    @GetMapping()
    public List<User> getAllUsers() {
        return service.getAllUsers();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable("id") int id) {
        return service.getUserById(id);
    }

    @PostMapping
    public ResponseEntity<String> create(@RequestBody User user) {
        service.saveOrUpdate(user);
        return ResponseEntity.ok("User created!");
    }

    @PutMapping
    public ResponseEntity<String> update(@RequestBody User user) {
        service.saveOrUpdate(user);
        return ResponseEntity.ok("User updated!");
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<String> delete(@PathVariable("id") int id) {
        service.delete(id);
        return ResponseEntity.ok("User deleted!");
    }
}
